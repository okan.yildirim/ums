package com.okan.ums.model.exception;

public class UmsBusinessException extends BaseUmsException {

    public UmsBusinessException(String key) {
        super(key);
    }

    public UmsBusinessException(String key, String... args) {
        super(key, args);
    }
}
