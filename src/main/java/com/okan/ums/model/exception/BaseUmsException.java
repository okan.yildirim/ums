package com.okan.ums.model.exception;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseUmsException extends RuntimeException {

    private final String key;
    private final String[] args;

    public BaseUmsException(String key) {
        this.key = key;
        this.args = ArrayUtils.EMPTY_STRING_ARRAY;
    }

    public BaseUmsException(String key, String... args) {
        this.key = key;
        this.args = args;
    }

    public String getKey() {
        return this.key;
    }

    public String[] getArgs() {
        return args;
    }
}
