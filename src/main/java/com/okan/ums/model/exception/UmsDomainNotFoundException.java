package com.okan.ums.model.exception;

public class UmsDomainNotFoundException extends BaseUmsException {

    public UmsDomainNotFoundException(String key) {
        super(key);
    }

    public UmsDomainNotFoundException(String key, String... args) {
        super(key, args);
    }
}
