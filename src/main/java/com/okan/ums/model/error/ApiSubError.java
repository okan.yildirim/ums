package com.okan.ums.model.error;


import org.apache.commons.lang3.ArrayUtils;

public class ApiSubError {
    private String field;
    private String message;
    private String key;
    private String[] args = ArrayUtils.EMPTY_STRING_ARRAY;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
