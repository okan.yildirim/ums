package com.okan.ums.model.request;

import javax.validation.constraints.NotEmpty;

public class UserEditRequest {

    private String name;
    private String surname;

    @NotEmpty(message = "user.request.username.is.empty")
    private String username;

    @NotEmpty(message = "user.request.email.is.empty")
    private String email;

    private int age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
