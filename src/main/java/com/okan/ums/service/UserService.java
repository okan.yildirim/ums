package com.okan.ums.service;

import com.okan.ums.entity.User;
import com.okan.ums.mapper.UserMapper;
import com.okan.ums.model.PageableResult;
import com.okan.ums.model.exception.UmsBusinessException;
import com.okan.ums.model.request.UserEditRequest;
import com.okan.ums.model.request.UserRequest;
import com.okan.ums.model.response.UserResponse;
import com.okan.ums.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public void create(UserRequest userRequest) {

        if (userRepository.existsByEmail(userRequest.getEmail())){
            throw new UmsBusinessException("email.is.already.exists");
        } else if (userRepository.existsByUsername(userRequest.getUsername())){
            throw new UmsBusinessException("username.is.already.exists");
        }

        User user = userMapper.toUser(userRequest);
        userRepository.save(user);
    }

    public PageableResult<UserResponse> getUsers(Pageable pageable) {
        Page<User> userPage = userRepository.findAll(pageable);
        List<UserResponse> userResponses = userPage.getContent().stream()
                .map(userMapper::toUserResponse)
                .collect(Collectors.toList());
        return new PageableResult<>(userPage.getTotalElements(), userPage.getNumber(), pageable.getPageSize(), userResponses);
    }

    public UserResponse getUser(Long id) {
        User user = getUserById(id);
        return userMapper.toUserResponse(user);
    }

    public void editUser(UserEditRequest userEditRequest, Long id) {
        User user = getUserById(id);
        userRepository.save(userMapper.toUser(userEditRequest, user));
    }

    private User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
