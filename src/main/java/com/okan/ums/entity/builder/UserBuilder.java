package com.okan.ums.entity.builder;

import com.okan.ums.entity.User;

import java.util.Date;

public final class UserBuilder {
    private String name;
    private String surname;
    private Date createdDate = new Date();
    private String username;
    private String email;
    private Date lastModifiedDate = new Date();
    private String password;
    private int age;

    private UserBuilder() {
    }

    public static UserBuilder anUser() {
        return new UserBuilder();
    }

    public UserBuilder name(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder surname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserBuilder createdDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public UserBuilder username(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder lastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public UserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder age(int age) {
        this.age = age;
        return this;
    }

    public User build() {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setCreatedDate(createdDate);
        user.setUsername(username);
        user.setEmail(email);
        user.setLastModifiedDate(lastModifiedDate);
        user.setPassword(password);
        user.setAge(age);
        return user;
    }
}
