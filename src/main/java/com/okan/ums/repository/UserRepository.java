package com.okan.ums.repository;

import com.okan.ums.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    Page<User> findAll(Pageable pageable);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
