package com.okan.ums.configuration.swagger;

import com.okan.ums.configuration.swagger.properties.Host;
import com.okan.ums.configuration.swagger.properties.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.Contact;

@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {

    @Value("${spring.application.name:Api Documentation}")
    private String appName;
    @Value("${spring.application.description}")
    private String appDescription;
    private String version;

    private Host host;

    private Contact contact;

    private License license;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppDescription() {
        return appDescription;
    }

    public void setAppDescription(String appDescription) {
        this.appDescription = appDescription;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }
}
