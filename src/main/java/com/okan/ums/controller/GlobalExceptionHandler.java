package com.okan.ums.controller;

import com.okan.ums.model.error.ApiError;
import com.okan.ums.model.error.ApiSubError;
import com.okan.ums.model.exception.UmsBusinessException;
import com.okan.ums.model.exception.UmsDomainNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.net.SocketTimeoutException;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

@RestControllerAdvice
public class GlobalExceptionHandler {

    Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final Locale TR = new Locale("tr");
    private static final String TIMEOUT_KEY = "ums.api.timeout";

    private final MessageSource messageSource;

    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(UmsDomainNotFoundException.class)
    public ResponseEntity<ApiError> handleDomainNotFoundException(UmsDomainNotFoundException exception) {

        ApiError apiError = new ApiError();
        apiError.setException("DomainNotFoundException");
        ApiSubError subError = new ApiSubError();
        subError.setMessage(getMessage(exception.getKey(), exception.getArgs(), StringUtils.EMPTY));
        subError.setKey(exception.getKey());
        apiError.addSubError(subError);

        LOGGER.error("TrendyolDomainNotFoundException Caused By: {}", apiError, exception);

        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UmsBusinessException.class)
    public ResponseEntity<ApiError> handleUmsBusinessException(UmsBusinessException umsBusinessException) {

        ApiError apiError = new ApiError();
        apiError.setException("umsBusinessException");

        ApiSubError subError = new ApiSubError();
        subError.setMessage(getMessage(umsBusinessException.getKey(), umsBusinessException.getArgs(), StringUtils.EMPTY));
        subError.setKey(umsBusinessException.getKey());
        apiError.addSubError(subError);

        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        ApiError apiError = new ApiError();
        apiError.setException("MethodArgumentNotValidException");

        exception.getBindingResult().getFieldErrors().forEach(i -> {

            ApiSubError subError = new ApiSubError();
            subError.setMessage(getMessage(i.getDefaultMessage(), i.getArguments(), StringUtils.EMPTY));
            subError.setKey(i.getField());
            apiError.addSubError(subError);
        });

        LOGGER.error("Field validation failed. Caused By: {}", apiError, exception);

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiError> handleConstraintViolationExceptionException(ConstraintViolationException exception) {
        ApiError apiError = new ApiError();
        apiError.setException("MethodArgumentNotValidException");

        exception.getConstraintViolations().forEach(constraintViolation -> {
            ApiSubError subError = new ApiSubError();
            subError.setMessage(getMessage(constraintViolation.getMessage(), constraintViolation.getInvalidValue()));
            subError.setKey(constraintViolation.getMessageTemplate());
            apiError.addSubError(subError);
        });

        LOGGER.error("Field validation failed. Caused By: {}", apiError, exception);

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ApiError> handleGenericException(Exception exception) {
        ApiError apiError = new ApiError();
        apiError.setException("GenericException");
        ApiSubError apiSubError = new ApiSubError();
        apiSubError.setMessage("Unexpected exception occurred. Try again later.");
        apiSubError.setKey("generic.error");
        apiError.addSubError(apiSubError);
        LOGGER.error("Generic Exception Occurred.", exception);
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(SocketTimeoutException.class)
    public ResponseEntity<ApiError> handleSocketTimeoutException(SocketTimeoutException exception) {
        ApiError apiError = new ApiError();
        apiError.setException("ClientApiBusinessException");

        ApiSubError fieldError = new ApiSubError();
        fieldError.setMessage(getMessage(TIMEOUT_KEY));
        fieldError.setKey(TIMEOUT_KEY);
        fieldError.setArgs(null);
        apiError.addSubError(fieldError);

        LOGGER.error("SocketTimeoutException Caused By: {}", apiError, exception);

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    private String getMessage(String key, Object[] args, String defaultMessage) {
        return Optional.of(getMessage(() -> messageSource.getMessage(key, args, TR), defaultMessage))
                .filter(StringUtils::isNotBlank)
                .orElse(defaultMessage);
    }

    private String getMessage(String key, Object... args) {
        return Optional.of(getMessage(() -> messageSource.getMessage(key, args, TR), StringUtils.EMPTY))
                .filter(StringUtils::isNotBlank)
                .orElse(StringUtils.EMPTY);
    }

    private String getMessage(Supplier<String> supplier, String defaultMessage) {
        String message = StringUtils.EMPTY;
        try {
            message = supplier.get();
        } catch (Exception exception) {
            if (Objects.isNull(defaultMessage)) {
                LOGGER.error("MessageResource Not found: {}", exception);
            }
        }
        return message;
    }

}
