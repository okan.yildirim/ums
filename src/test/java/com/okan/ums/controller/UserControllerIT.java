package com.okan.ums.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.ums.model.PageableResult;
import com.okan.ums.model.request.UserEditRequest;
import com.okan.ums.model.request.UserRequest;
import com.okan.ums.model.response.UserResponse;
import com.okan.ums.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Test
    void it_should_create_user() throws Exception {
        //Given
        var userRequest = new UserRequest();
        userRequest.setEmail("okan@email.com");
        userRequest.setSurname("yildirim");
        userRequest.setName("okan");
        userRequest.setPassword("123");
        userRequest.setAge(25);
        userRequest.setUsername("okan.yildirim");

        var objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(userRequest);
        //When
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)).andExpect(status().isCreated());
        //Then
        var userRequestArgumentCaptor = ArgumentCaptor.forClass(UserRequest.class);
        verify(userService).create(userRequestArgumentCaptor.capture());
        var capturedUserRequest = userRequestArgumentCaptor.getValue();
        assertThat(capturedUserRequest).isEqualToComparingFieldByField(userRequest);
    }

    @Test
    void it_should_return_bad_request_when_request_is_not_valid() throws Exception {
        //Given
        var userRequest = new UserRequest();

        var objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(userRequest);
        //When
        ResultActions result = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request));
        //Then
        result.andExpect(status().isBadRequest());
    }

    @Test
    void it_should_get_users() throws Exception {
        //Given
        var userResponse = new UserResponse();
        userResponse.setEmail("okan@email.com");
        userResponse.setSurname("yildirim");
        userResponse.setName("okan");
        userResponse.setAge(25);

        PageableResult<UserResponse> pageableResult = new PageableResult<>(1, 0, 1, List.of(userResponse));
        var objectMapper = new ObjectMapper();
        String expectedResponse = objectMapper.writeValueAsString(pageableResult);

        when(userService.getUsers(PageRequest.of(0, 1))).thenReturn(pageableResult);
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("page", "0");
        params.add("size", "1");
        //When
        ResultActions resultActions = mockMvc.perform(get("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .params(params)).andExpect(status().isOk());

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page").value(0))
                .andExpect(jsonPath("$.size").value(1))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.content").isNotEmpty())
                .andExpect(jsonPath("$.content[0].email").value(userResponse.getEmail()))
                .andExpect(jsonPath("$.content[0].surname").value(userResponse.getSurname()))
                .andExpect(jsonPath("$.content[0].username").value(userResponse.getUsername()))
                .andExpect(jsonPath("$.content[0].age").value(userResponse.getAge()));
    }

    @Test
    void it_should_get_user() throws Exception {
        //Given
        var userResponse = new UserResponse();
        userResponse.setEmail("okan@email.com");
        userResponse.setSurname("yildirim");
        userResponse.setName("okan");
        userResponse.setAge(25);

        var objectMapper = new ObjectMapper();
        String expectedResponse = objectMapper.writeValueAsString(userResponse);

        long userId = 1L;
        when(userService.getUser(userId)).thenReturn(userResponse);

        //When
        ResultActions resultActions = mockMvc.perform(get("/users/" + userId))
                .andExpect(status().isOk());

        //Then
        String response = resultActions.andReturn().getResponse().getContentAsString();
        assertThat(response).isEqualTo(expectedResponse);
    }

    @Test
    void it_should_edit_user() throws Exception {
        //Given
        var userEditRequest = new UserEditRequest();
        userEditRequest.setEmail("okan@email.com");
        userEditRequest.setSurname("yildirim");
        userEditRequest.setName("okan");
        userEditRequest.setAge(25);
        userEditRequest.setUsername("okan.yildirim");
        long userId = 1L;

        var objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(userEditRequest);
        //When
        mockMvc.perform(put("/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)).andExpect(status().isOk());
        //Then
        var userRequestArgumentCaptor = ArgumentCaptor.forClass(UserEditRequest.class);
        verify(userService).editUser(userRequestArgumentCaptor.capture(), eq(userId));
        var capturedUserRequest = userRequestArgumentCaptor.getValue();
        assertThat(capturedUserRequest).isEqualToComparingFieldByField(userEditRequest);
    }

    @Test
    void it_should_return_bad_request_when_edit_user() throws Exception {
        //Given
        var userEditRequest = new UserEditRequest();
        long userId = 1L;

        var objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(userEditRequest);
        //When
        ResultActions result = mockMvc.perform(put("/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(request));
        //Then
        result.andExpect(status().isBadRequest());
    }

    @Test
    void it_should_delete_user() throws Exception {
        //Given
        long userId = 1L;
        //When
        mockMvc.perform(delete("/users/" + userId))
                .andExpect(status().isOk());

        //Then
       verify(userService).deleteUser(userId);
    }
}