package com.okan.ums.controller;

import com.okan.ums.model.PageableResult;
import com.okan.ums.model.request.UserEditRequest;
import com.okan.ums.model.request.UserRequest;
import com.okan.ums.model.response.UserResponse;
import com.okan.ums.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Test
    void it_should_create_user() {
        // Given
        var userRequest = new UserRequest();
        userRequest.setEmail("okan@email.com");
        userRequest.setSurname("yildirim");
        userRequest.setName("okan");
        userRequest.setPassword("123");
        userRequest.setAge(25);
        userRequest.setUsername("okan.yildirim");
        // When
        userController.createUser(userRequest);
        // Then
        var userRequestArgumentCaptor = ArgumentCaptor.forClass(UserRequest.class);
        verify(userService).create(userRequestArgumentCaptor.capture());
        var capturedUserRequest = userRequestArgumentCaptor.getValue();
        assertThat(capturedUserRequest).isEqualToComparingFieldByField(userRequest);
    }

    @Test
    void it_should_get_users() {
        // Given
        Pageable pageable = Pageable.unpaged();
        PageableResult pageableResult = PageableResult.getEmptyPageableResult();
        when(userService.getUsers(pageable)).thenReturn(pageableResult);
        // When
        PageableResult result = userController.getUsers(pageable);
        // Then
        verify(userService).getUsers(pageable);
        assertThat(result).isEqualTo(pageableResult);
    }

    @Test
    void it_should_get_user() {
        // Given
        long id = 1L;
        UserResponse userResponse = new UserResponse();
        when(userService.getUser(id)).thenReturn(userResponse);
        // When
        UserResponse response = userController.getUser(id);
        // Then
        verify(userService).getUser(id);
        assertThat(response).isEqualTo(userResponse);
    }

    @Test
    void it_should_edit_user() {
        // Given
        long id = 1L;
        var userRequest = new UserEditRequest();
        // When
        userController.editUser(userRequest, id);
        // Then
        verify(userService).editUser(userRequest, id);
    }

    @Test
    void it_should_delete_user() {
        // Given
        long id = 1L;
        // When
        userController.deleteUser(id);
        // Then
        verify(userService).deleteUser(id);
    }
}