package com.okan.ums.controller;

import com.okan.ums.model.error.ApiError;
import com.okan.ums.model.error.ApiSubError;
import com.okan.ums.model.exception.UmsBusinessException;
import com.okan.ums.model.exception.UmsDomainNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GlobalExceptionHandlerTest {

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @Mock
    private MessageSource messageSource;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;

    @Test
    public void should_handle_business_exception() {
        var exception = new UmsBusinessException("ums.api.exception");

        when(messageSource.getMessage(exception.getKey(), new Object[]{}, new Locale("tr"))).thenReturn("message");

        ResponseEntity<?> responseEntity = globalExceptionHandler.handleUmsBusinessException(exception);

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(responseEntity.getBody())
                .isNotNull()
                .isInstanceOf(ApiError.class);
        ApiError apiError = (ApiError) responseEntity.getBody();
        assertThat(apiError.getSubErrors()).hasSize(1);
        assertThat(apiError.getSubErrors().get(0).getMessage()).isEqualTo("message");
        assertThat(apiError.getSubErrors().get(0).getKey()).isEqualTo("ums.api.exception");
    }

    @Test
    public void should_handle_domain_not_found_exception() {
        UmsDomainNotFoundException exception = new UmsDomainNotFoundException("ums.api.exception");

        when(messageSource.getMessage(exception.getKey(), new Object[]{}, new Locale("tr"))).thenReturn("message");

        ResponseEntity<?> responseEntity = globalExceptionHandler.handleDomainNotFoundException(exception);

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseEntity.getBody())
                .isNotNull()
                .isInstanceOf(ApiError.class);
        ApiError apiError = (ApiError) responseEntity.getBody();
        assertThat(apiError.getSubErrors()).hasSize(1);
        assertThat(apiError.getSubErrors().get(0).getMessage()).isEqualTo("message");
        assertThat(apiError.getSubErrors().get(0).getKey()).isEqualTo("ums.api.exception");
    }

    @Test
    public void should_handle_method_argument_not_valid_exception() throws Exception {

        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

        List<FieldError> fieldErrors = new ArrayList<>();
        FieldError fieldError1 = new FieldError("orderRequest", "orderNumber", "orderNumberMessageError");
        FieldError fieldError2 = new FieldError("orderRequest", "customerName", "customerNameMessageError");
        fieldErrors.add(fieldError1);
        fieldErrors.add(fieldError2);

        when(messageSource.getMessage(fieldError1.getDefaultMessage(), fieldError1.getArguments(), new Locale("tr"))).thenReturn("first localised Message");
        when(messageSource.getMessage(fieldError2.getDefaultMessage(), fieldError2.getArguments(), new Locale("tr"))).thenReturn("second localised Message");


        when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

        ResponseEntity<?> responseEntity = globalExceptionHandler.handleMethodArgumentNotValidException(methodArgumentNotValidException);

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody())
                .isNotNull()
                .isInstanceOf(ApiError.class);
        ApiError apiError = (ApiError) responseEntity.getBody();
        assertThat(apiError.getException()).isEqualTo("MethodArgumentNotValidException");

        assertThat(apiError.getSubErrors().size()).isEqualTo(2);

        ApiSubError fieldApiError1 = apiError.getSubErrors().get(0);
        ApiSubError fieldApiError2 = apiError.getSubErrors().get(1);

        assertThat(fieldApiError1.getMessage()).isEqualTo("first localised Message");
        assertThat(fieldApiError1.getKey()).isEqualTo("orderNumber");

        assertThat(fieldApiError2.getMessage()).isEqualTo("second localised Message");
        assertThat(fieldApiError2.getKey()).isEqualTo("customerName");


        verify(messageSource).getMessage(fieldError1.getDefaultMessage(), fieldError1.getArguments(), new Locale("tr"));
        verify(messageSource).getMessage(fieldError2.getDefaultMessage(), fieldError2.getArguments(), new Locale("tr"));
    }
}