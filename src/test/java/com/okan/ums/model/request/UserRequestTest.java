package com.okan.ums.model.request;

import org.junit.jupiter.api.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class UserRequestTest {

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    void it_should_validate() {
        // given
        UserRequest userRequest = new UserRequest();
        // when
        var validationResult = validator.validate(userRequest).stream()
                .collect(Collectors.toMap(field -> field.getPropertyPath().toString(), error -> error));
        //then
        assertThat(validationResult).hasSize(3);
        assertThat(validationResult.get("username").getMessage()).isEqualTo("user.request.username.is.empty");
        assertThat(validationResult.get("email").getMessage()).isEqualTo("user.request.email.is.empty");
        assertThat(validationResult.get("password").getMessage()).isEqualTo("user.request.password.is.empty");
    }
}