package com.okan.ums.repository;

import com.okan.ums.entity.User;
import com.okan.ums.entity.builder.UserBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void it_should_save_and_find_by_id() {
        //Given
        User user = UserBuilder.anUser()
                .username("username")
                .age(25)
                .email("email@email.com")
                .name("name")
                .password("password")
                .surname("surname")
                .build();
        //When
        User savedUser = userRepository.save(user);
        Optional<User> fetchedUser = userRepository.findById(savedUser.getId());
        //Then
        assertThat(fetchedUser.isPresent()).isTrue();
        assertThat(fetchedUser.get()).isEqualTo(user);
    }

    @Test
    void it_should_throw_exception_when_email_is_not_valid_format() {
        //Given
        User user = UserBuilder.anUser()
                .username("username")
                .age(25)
                .email("email")
                .name("name")
                .password("password")
                .surname("surname")
                .build();
        //When
        Throwable throwable = catchThrowable(() -> userRepository.save(user));
        //Then
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            userRepository.save(user);
            userRepository.findAll();
        });
    }

    @Test
    @Sql(statements = "INSERT INTO USERS(id, name, surname, username, email, password, age, created_date, last_modified_date) VALUES(1, 'okan', 'yildirim','okan.yildirim','okan@email.com','123',25, '2020-03-15','2020-03-15')")
    void it_should_not_save_and_when_exists_duplicate_username() {
        //Given
        User userWithDuplicateUserName = UserBuilder.anUser()
                .username("okan.yildirim")
                .age(25)
                .email("email@email.com")
                .name("name")
                .password("password")
                .surname("surname")
                .build();

        User userWithDuplicateEmail = UserBuilder.anUser()
                .username("username")
                .age(25)
                .email("okan@email.com")
                .name("name")
                .password("password")
                .surname("surname")
                .build();

        //When
        Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            userRepository.save(userWithDuplicateUserName);
            userRepository.save(userWithDuplicateEmail);
            userRepository.findAll();
        });
    }

    @Test
    @Sql(scripts = "classpath:/sql/insert_ten_users.sql")
    void it_should_find_all_pageable() {
        //Given
        //When
        Page<User> secondPage = userRepository.findAll(PageRequest.of(1, 5));
        List<User> content = secondPage.getContent();

        assertThat(secondPage.getTotalElements()).isEqualTo(10);
        assertThat(secondPage.getNumber()).isEqualTo(1);
        assertThat(secondPage.getTotalPages()).isEqualTo(2);
        assertThat(content.get(0).getName()).isEqualTo("name5");
    }
}