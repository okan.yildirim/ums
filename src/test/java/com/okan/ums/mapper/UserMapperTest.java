package com.okan.ums.mapper;

import com.okan.ums.entity.User;
import com.okan.ums.model.request.UserRequest;
import com.okan.ums.model.response.UserResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper;

    @Test
    void should_transform_from_user_to_user_response() {
        // Given
        User user = getUser();
        // When
        UserResponse userResponse = userMapper.toUserResponse(user);
        // Then
        assertThat(userResponse).isEqualToComparingOnlyGivenFields(user);
    }

    @Test
    void should_transform_from_user_request_to_user() {
        // Given
        UserRequest userRequest = getUserRequest();
        // When
        User user = userMapper.toUser(userRequest);
        // Then
        assertThat(user).isEqualToComparingOnlyGivenFields(user);
    }

    private User getUser() {
        User user = new User();
        user.setUsername("username");
        user.setEmail("email");
        user.setPassword("password");
        user.setName("name");
        user.setSurname("surname");
        user.setAge(5);
        return user;
    }

    private UserRequest getUserRequest() {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername("username");
        userRequest.setEmail("email");
        userRequest.setPassword("password");
        userRequest.setName("name");
        userRequest.setSurname("surname");
        userRequest.setAge(5);
        return userRequest;
    }
}