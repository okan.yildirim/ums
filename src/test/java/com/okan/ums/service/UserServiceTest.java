package com.okan.ums.service;

import com.okan.ums.entity.User;
import com.okan.ums.mapper.UserMapper;
import com.okan.ums.model.PageableResult;
import com.okan.ums.model.request.UserEditRequest;
import com.okan.ums.model.request.UserRequest;
import com.okan.ums.model.response.UserResponse;
import com.okan.ums.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Test
    void it_should_create_user() {
        // Given
        UserRequest userRequest = getUserRequest();
        User user = getUser();
        when(userMapper.toUser(userRequest)).thenReturn(user);
        // When
        userService.create(userRequest);
        // Then
        verify(userMapper).toUser(userRequest);
        verify(userRepository).save(user);
    }


    @Test
    void it_should_get_users() {
        // Given
        PageRequest pageRequest = PageRequest.of(1, 10);
        Page<User> users = Page.empty(pageRequest);

        when(userRepository.findAll(pageRequest)).thenReturn(users);
        // When
        PageableResult result = userService.getUsers(pageRequest);
        // Then
        // verify(userMapper, times(10)).toUserResponse(any(User.class));
        verify(userRepository).findAll(pageRequest);
        assertThat(result.getPage()).isEqualTo(1);
        assertThat(result.getSize()).isEqualTo(10);
    }

    @Test
    void it_should_get_user() {
        // Given
        long id = 1L;
        UserResponse userResponse = getUserResponse();
        User user = getUser();
        when(userRepository.findById(id)).thenReturn(Optional.of(user));
        when(userMapper.toUserResponse(user)).thenReturn(userResponse);
        // When
        UserResponse response = userService.getUser(id);
        // Then
        verify(userRepository).findById(id);
        verify(userMapper).toUserResponse(user);
        assertThat(response).isEqualTo(userResponse);
    }

    @Test
    void it_should_edit_user() {
        // Given
        long id = 1L;
        UserEditRequest userRequest = getUserEditRequest();
        User user = getUser();
        when(userRepository.findById(id)).thenReturn(Optional.of(user));
        when(userMapper.toUser(userRequest, user)).thenReturn(user);
        // When
        userService.editUser(userRequest, id);
        // Then
        verify(userRepository).findById(id);
        verify(userRepository).save(user);
    }

    @Test
    void it_should_delete_user() {
        // Given
        long id = 1L;
        // When
        userService.deleteUser(id);
        // Then
        verify(userRepository).deleteById(id);
    }

    private User getUser() {
        var user = new User();
        user.setUsername("username");
        user.setEmail("email");
        user.setPassword("password");
        user.setName("name");
        user.setSurname("surname");
        user.setAge(5);
        return user;
    }

    private UserRequest getUserRequest() {
        var userRequest = new UserRequest();
        userRequest.setUsername("username");
        userRequest.setEmail("email");
        userRequest.setPassword("password");
        userRequest.setName("name");
        userRequest.setSurname("surname");
        userRequest.setAge(5);
        return userRequest;
    }

    private UserEditRequest getUserEditRequest() {
        var userRequest = new UserEditRequest();
        userRequest.setUsername("username");
        userRequest.setEmail("email");
        userRequest.setName("name");
        userRequest.setSurname("surname");
        userRequest.setAge(5);
        return userRequest;
    }

    private UserResponse getUserResponse() {
        var userResponse = new UserResponse();
        userResponse.setUsername("username");
        userResponse.setEmail("email");
        userResponse.setName("name");
        userResponse.setSurname("surname");
        userResponse.setAge(5);
        return userResponse;

    }
}
